# vRA 8 Beta

Blueprints for vRA 8 Beta

vRA8BetaUC1 is a single-node blueprint with simple input parameters and the ability to scale node count

vRA8BetaUC2 is a complex blueprint with advanced input paraneters and a load balanced, clustered front end/database back end, ultimately resulting in a hosted eCommerce app

vRA8BetaUC2-Fixed is a version of the UC2 blueprint with the input parameter updates already fixed (for exercise 4)